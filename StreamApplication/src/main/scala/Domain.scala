

object Domain {

  val HEAD: String = "Flow ID,Src IP,Src Port,Dst IP,Dst Port,Protocol,Timestamp,Flow Duration,Total Fwd Packet,Total Bwd packets,Total Length of Fwd Packet,Total Length of Bwd Packet,Fwd Packet Length Max,Fwd Packet Length Min,Fwd Packet Length Mean,Fwd Packet Length Std,Bwd Packet Length Max,Bwd Packet Length Min,Bwd Packet Length Mean,Bwd Packet Length Std,Flow Bytes/s,Flow Packets/s,Flow IAT Mean,Flow IAT Std,Flow IAT Max,Flow IAT Min,Fwd IAT Total,Fwd IAT Mean,Fwd IAT Std,Fwd IAT Max,Fwd IAT Min,Bwd IAT Total,Bwd IAT Mean,Bwd IAT Std,Bwd IAT Max,Bwd IAT Min,Fwd PSH Flags,Bwd PSH Flags,Fwd URG Flags,Bwd URG Flags,Fwd Header Length,Bwd Header Length,Fwd Packets/s,Bwd Packets/s,Packet Length Min,Packet Length Max,Packet Length Mean,Packet Length Std,Packet Length Variance,FIN Flag Count,SYN Flag Count,RST Flag Count,PSH Flag Count,ACK Flag Count,URG Flag Count,CWR Flag Count,ECE Flag Count,Down/Up Ratio,Average Packet Size,Fwd Segment Size Avg,Bwd Segment Size Avg,Fwd Bytes/Bulk Avg,Fwd Packet/Bulk Avg,Fwd Bulk Rate Avg,Bwd Bytes/Bulk Avg,Bwd Packet/Bulk Avg,Bwd Bulk Rate Avg,Subflow Fwd Packets,Subflow Fwd Bytes,Subflow Bwd Packets,Subflow Bwd Bytes,FWD Init Win Bytes,Bwd Init Win Bytes,Fwd Act Data Pkts,Fwd Seg Size Min,Active Mean,Active Std,Active Max,Active Min,Idle Mean,Idle Std,Idle Max,Idle Min,Label"

  type FLOWID = String
  type SRCIP = String
  type SRCPORT = Long
  type DSTIP = String
  type DSTPORT = Long
  type PROTOCOL = Long
  type TIMESTAMP = String
  type FLOWDURATION = Long
  type TOTALFWDPACKET = Long
  type TOTALBWDPACKETS = Long
  type TOTALLENGTHOFFWDPACKET = Double
  type TOTALLENGTHOFBWDPACKET = Double
  type FWDPACKETLENGTHMAX = Double
  type FWDPACKETLENGTHMIN = Double
  type FWDPACKETLENGTHMEAN = Double
  type FWDPACKETLENGTHSTD = Double
  type BWDPACKETLENGTHMAX = Long
  type BWDPACKETLENGTHMIN = Long
  type BWDPACKETLENGTHMEAN = Long
  type BWDPACKETLENGTHSTD = Long
  type FLOWBYTES_S = Double
  type FLOWPACKETS_S = Double
  type FLOWIATMEAN = Double
  type FLOWIATSTD = Double
  type FLOWIATMAX = Double
  type FLOWIATMIN = Double
  type FWDIATTOTAL = Double
  type FWDIATMEAN = Double
  type FWDIATSTD = Double
  type FWDIATMAX = Double
  type FWDIATMIN = Double
  type BWDIATTOTAL = Long
  type BWDIATMEAN = Long
  type BWDIATSTD = Long
  type BWDIATMAX = Long
  type BWDIATMIN = Long
  type FWDPSHFLAGS = Long
  type BWDPSHFLAGS = Long
  type FWDURGFLAGS = Long
  type BWDURGFLAGS = Long
  type FWDHEADERLENGTH = Long
  type BWDHEADERLENGTH = Long
  type FWDPACKETS_S = Double
  type BWDPACKETS_S = Double
  type PACKETLENGTHMIN = Double
  type PACKETLENGTHMAX = Double
  type PACKETLENGTHMEAN = Double
  type PACKETLENGTHSTD = Double
  type PACKETLENGTHVARIANCE = Double
  type FINFLAGCOUNT = Long
  type SYNFLAGCOUNT = Long
  type RSTFLAGCOUNT = Long
  type PSHFLAGCOUNT = Long
  type ACKFLAGCOUNT = Long
  type URGFLAGCOUNT = Long
  type CWRFLAGCOUNT = Long
  type ECEFLAGCOUNT = Long
  type DOWN_UPRATIO = Double
  type AVERAGEPACKETSIZE = Double
  type FWDSEGMENTSIZEAVG = Double
  type BWDSEGMENTSIZEAVG = Double
  type FWDBYTES_BULKAVG = Long
  type FWDPACKET_BULKAVG = Long
  type FWDBULKRATEAVG = Long
  type BWDBYTES_BULKAVG = Long
  type BWDPACKET_BULKAVG = Long
  type BWDBULKRATEAVG = Long
  type SUBFLOWFWDPACKETS = Long
  type SUBFLOWFWDBYTES = Long
  type SUBFLOWBWDPACKETS = Long
  type SUBFLOWBWDBYTES = Long
  type FWDINITWINBYTES = Long
  type BWDINITWINBYTES = Long
  type FWDACTDATAPKTS = Long
  type FWDSEGSIZEMIN = Long
  type ACTIVEMEAN = Long
  type ACTIVESTD = Long
  type ACTIVEMAX = Long
  type ACTIVEMIN = Long
  type IDLEMEAN = Long
  type IDLESTD = Long
  type IDLEMAX = Long
  type IDLEMIN = Long
  type LABEL = String

      case class Packet(Flow_ID: FLOWID, //0
                        Src_IP: SRCIP, //1
                        Src_Port: SRCPORT, //2
                        Dst_IP: DSTIP, //3
                        Dst_Port: DSTPORT, //4
                        Protocol: PROTOCOL, //5
                        Timestamp: TIMESTAMP, //6
                        Flow_Duration: FLOWDURATION, //7
                        Total_Fwd_Packet: TOTALFWDPACKET, //7
                        Total_Bwd_packets: TOTALBWDPACKETS, //9
                        Total_Length_of_Fwd_Packet: TOTALLENGTHOFFWDPACKET, //10
                        Total_Length_of_Bwd_Packet: TOTALLENGTHOFBWDPACKET, //11
                        Fwd_Packet_Length_Max: FWDPACKETLENGTHMAX, //12
                        Fwd_Packet_Length_Min: FWDPACKETLENGTHMIN, //13
                        Fwd_Packet_Length_Mean: FWDPACKETLENGTHMEAN, //14
                        Fwd_Packet_Length_Std: FWDPACKETLENGTHSTD, //15
                        Bwd_Packet_Length_Max: BWDPACKETLENGTHMAX, //16
                        Bwd_Packet_Length_Min: BWDPACKETLENGTHMIN, //17
                        Bwd_Packet_Length_Mean: BWDPACKETLENGTHMEAN, //18
                        Bwd_Packet_Length_Std: BWDPACKETLENGTHSTD, //19
                        Flow_Bytes_s: FLOWBYTES_S, //20
                        Flow_Packets_s: FLOWPACKETS_S, //21
                        Flow_IAT_Mean: FLOWIATMEAN, //22
                        Flow_IAT_Std: FLOWIATSTD, //23
                        Flow_IAT_Max: FLOWIATMAX, //24
                        Flow_IAT_Min: FLOWIATMIN, //25
                        Fwd_IAT_Total: FWDIATTOTAL, //26
                        Fwd_IAT_Mean: FWDIATMEAN, //27
                        Fwd_IAT_Std: FWDIATSTD, //28
                        Fwd_IAT_Max: FWDIATMAX, //29
                        Fwd_IAT_Min: FWDIATMIN, //30
                        Bwd_IAT_Total: BWDIATTOTAL, //31
                        Bwd_IAT_Mean: BWDIATMEAN, //32
                        Bwd_IAT_Std: BWDIATSTD, //33
                        Bwd_IAT_Max: BWDIATMAX, //34
                        Bwd_IAT_Min: BWDIATMIN, //35
                        Fwd_PSH_Flags: FWDPSHFLAGS, //36
                        Bwd_PSH_Flags: BWDPSHFLAGS, //37
                        Fwd_URG_Flags: FWDURGFLAGS, //38
                        Bwd_URG_Flags: BWDURGFLAGS, //39
                        Fwd_Header_Length: FWDHEADERLENGTH, //40
                        Bwd_Header_Length: BWDHEADERLENGTH, //41
                        Fwd_Packets_s: FWDPACKETS_S, //42
                        Bwd_Packets_s: BWDPACKETS_S, //43
                        Packet_Length_Min: PACKETLENGTHMIN, //44
                        Packet_Length_Max: PACKETLENGTHMAX, //45
                        Packet_Length_Mean: PACKETLENGTHMEAN, //46
                        Packet_Length_Std: PACKETLENGTHSTD, //47
                        Packet_Length_Variance: PACKETLENGTHVARIANCE, //48
                        FIN_Flag_Count: FINFLAGCOUNT, //49
                        SYN_Flag_Count: SYNFLAGCOUNT, //50
                        RST_Flag_Count: RSTFLAGCOUNT, //51
                        PSH_Flag_Count: PSHFLAGCOUNT, //52
                        ACK_Flag_Count: ACKFLAGCOUNT, //53
                        URG_Flag_Count: URGFLAGCOUNT, //54
                        CWR_Flag_Count: CWRFLAGCOUNT, //55
                        ECE_Flag_Count: ECEFLAGCOUNT, //56
                        Down_Up_Ratio: DOWN_UPRATIO, //57
                        Average_Packet_Size: AVERAGEPACKETSIZE, //58
                        Fwd_Segment_Size_Avg: FWDSEGMENTSIZEAVG, //59
                        Bwd_Segment_Size_Avg: BWDSEGMENTSIZEAVG, //60
                        Fwd_Bytes_Bulk_Avg: FWDBYTES_BULKAVG,  //61
                        Fwd_Packet_Bulk_Avg: FWDPACKET_BULKAVG, //62
                        Fwd_Bulk_Rate_Avg: FWDBULKRATEAVG, //63
                        Bwd_Bytes_Bulk_Avg: BWDBYTES_BULKAVG, //64
                        Bwd_Packet_Bulk_Avg: BWDPACKET_BULKAVG, //65
                        Bwd_Bulk_Rate_Avg: BWDBULKRATEAVG, //66
                        Subflow_Fwd_Packets: SUBFLOWFWDPACKETS, //67
                        Subflow_Fwd_Bytes: SUBFLOWFWDBYTES, //68
                        Subflow_Bwd_Packets: SUBFLOWBWDPACKETS, //69
                        Subflow_Bwd_Bytes: SUBFLOWBWDBYTES, //70
                        FWD_Init_Win_Bytes: FWDINITWINBYTES, //71
                        Bwd_Init_Win_Bytes: BWDINITWINBYTES, //72
                        Fwd_Act_Data_Pkts: FWDACTDATAPKTS, //73
                        Fwd_Seg_Size_Min: FWDSEGSIZEMIN, //74
                        Active_Mean: ACTIVEMEAN, //75
                        Active_Std: ACTIVESTD, //76
                        Active_Max: ACTIVEMAX, //77
                        Active_Min: ACTIVEMIN, //78
                        Idle_Mean: IDLEMEAN, //79
                        Idle_Std: IDLESTD, //80
                        Idle_Max: IDLEMAX, //81
                        Idle_Min: IDLEMIN, //82
                        Label: LABEL) //83

}


//val head = "Flow ID,Src IP,Src Port,Dst IP,Dst Port,Protocol,Timestamp,Flow Duration,Total Fwd Packet,Total Bwd packets,Total Length of Fwd Packet,Total Length of Bwd Packet,Fwd Packet Length Max,Fwd Packet Length Min,Fwd Packet Length Mean,Fwd Packet Length Std,Bwd Packet Length Max,Bwd Packet Length Min,Bwd Packet Length Mean,Bwd Packet Length Std,Flow Bytes/s,Flow Packets/s,Flow IAT Mean,Flow IAT Std,Flow IAT Max,Flow IAT Min,Fwd IAT Total,Fwd IAT Mean,Fwd IAT Std,Fwd IAT Max,Fwd IAT Min,Bwd IAT Total,Bwd IAT Mean,Bwd IAT Std,Bwd IAT Max,Bwd IAT Min,Fwd PSH Flags,Bwd PSH Flags,Fwd URG Flags,Bwd URG Flags,Fwd Header Length,Bwd Header Length,Fwd Packets/s,Bwd Packets/s,Packet Length Min,Packet Length Max,Packet Length Mean,Packet Length Std,Packet Length Variance,FIN Flag Count,SYN Flag Count,RST Flag Count,PSH Flag Count,ACK Flag Count,URG Flag Count,CWR Flag Count,ECE Flag Count,Down/Up Ratio,Average Packet Size,Fwd Segment Size Avg,Bwd Segment Size Avg,Fwd Bytes/Bulk Avg,Fwd Packet/Bulk Avg,Fwd Bulk Rate Avg,Bwd Bytes/Bulk Avg,Bwd Packet/Bulk Avg,Bwd Bulk Rate Avg,Subflow Fwd Packets,Subflow Fwd Bytes,Subflow Bwd Packets,Subflow Bwd Bytes,FWD Init Win Bytes,Bwd Init Win Bytes,Fwd Act Data Pkts,Fwd Seg Size Min,Active Mean,Active Std,Active Max,Active Min,Idle Mean,Idle Std,Idle Max,Idle Min,Label"
//val fea = head.split(",")
//val cty = fea.map(_.replaceAll("\\s", "").
//  replaceAll("/", "_").
//  toUpperCase)
//
//val ty = fea.map(_.replaceAll("\\s", "_").
//  replaceAll("/", "_"))
//
//cty.foreach(t => println("type " + t + " = String"))
//
//val cs = ty.zip(cty).map { case (v1, v2) => s"$v1: $v2"}
//
//cs.foreach(i => println(s"$i,"))