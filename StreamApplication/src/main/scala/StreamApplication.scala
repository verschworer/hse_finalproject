import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._
import io.circe.{Decoder, Encoder}
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.streams.kstream.{GlobalKTable, JoinWindows, TimeWindows, Windowed}
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala._
import org.apache.kafka.streams.scala.kstream.{KGroupedStream, KStream, KTable}
import org.apache.kafka.streams.scala.serialization.Serdes
import org.apache.kafka.streams.scala.serialization.Serdes._
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig, Topology}

import java.time.Duration
import java.time.temporal.ChronoUnit
import java.util.Properties
import scala.concurrent.duration._
import Domain._

object StreamApplication extends App {

  val TOPIC: String = "PACKETS"

  implicit def serde[A >: Null : Decoder : Encoder]: Serde[A] = {
    val serializer = (a: A) => a.asJson.noSpaces.getBytes
    val deserializer = (aAsBytes: Array[Byte]) => {
      val aAsString = new String(aAsBytes)
      val aOrError = decode[A](aAsString)
      aOrError match {
        case Right(a) => Option(a)
        case Left(error) =>
          println(s"error $aOrError, $error")
          Option.empty
      }
    }
    Serdes.fromFn[A](serializer, deserializer)
  }

  val builder: StreamsBuilder = new StreamsBuilder

  val PacketsStream: KStream[FLOWID, Packet] = builder.stream[FLOWID, Packet](TOPIC)

  val BROKER_LIST: String = "broker:9092"

  val config: Properties = new Properties()
  config.put(StreamsConfig.APPLICATION_ID_CONFIG, "stream-application")
  config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, BROKER_LIST)
  config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.stringSerde.getClass)
  config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.stringSerde.getClass)
//  config.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
//  config.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

  val topology: Topology = builder.build()
  println(topology.describe())

  val application: KafkaStreams = new KafkaStreams(topology, config)
  application.start()

}