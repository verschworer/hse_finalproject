import sbt.Keys.libraryDependencies

name := "HSE_FinalProject"

version := "1.0"

scalaVersion := "2.12.15"

def assemblysettings() = Seq(

  assembly / assemblyMergeStrategy := {
    case PathList("META-INF", _@_*) => MergeStrategy.discard
    case _ => MergeStrategy.first
  }
)

def dockerSettingsProd() = Seq(

  docker / dockerfile := {
    val artifact: File = assembly.value
    val artifactTargetPath = s"/project/${artifact.name}"
    val scriptSourceDir = baseDirectory.value / "../scripts"
    val projectDir = "/project/"

    new Dockerfile {
      from("verschworer/prod_app:latest")
//      from("verschworer:latest")
      add(artifact, artifactTargetPath)
      copy(scriptSourceDir, projectDir)
      run("mkdir", "-p", "/CSVs")
      run("chmod", "500", "/project/start.sh")
      run("chmod", "500", "/project/wait-for-it.sh")
      entryPoint("/project/start.sh")
      cmd(projectDir, s"${name.value}", s"${version.value}")
    }
  },
    docker / imageNames := Seq(
    ImageName(s"verschworer/${name.value}:latest")
  )
)

def dockerSettingsCons() = Seq(

  docker / dockerfile := {
    val artifact: File = assembly.value
    val artifactTargetPath = s"/project/${artifact.name}"
    val scriptSourceDir = baseDirectory.value / "../scripts"
    val projectDir = "/project/"

    new Dockerfile {
      from("bitnami/spark:latest")
      add(artifact, artifactTargetPath)
      copy(scriptSourceDir, projectDir)
      entryPoint("/opt/bitnami/scripts/spark/entrypoint.sh")
      cmd("spark-submit",
        "/project/spark-stream-app-assembly-1.0.jar",
        "--packages",
        "org.apache.spark:spark-sql-kafka-0-10_2.12:3.2.1")
    }
  },
  docker / imageNames := Seq(
    ImageName(s"verschworer/${name.value}:latest")
  )
)

lazy val stream_app = (project in file("StreamApplication"))
  .enablePlugins(sbtdocker.DockerPlugin)
  .settings(
    libraryDependencies ++= Seq(
      "org.apache.kafka" % "kafka-clients" % "3.1.0",
      "org.apache.kafka" % "kafka-streams" % "3.1.0",
      "org.apache.kafka" %% "kafka-streams-scala" % "3.1.0",
      "io.circe" %% "circe-core" % "0.14.1",
      "io.circe" %% "circe-generic" % "0.14.1",
      "io.circe" %% "circe-parser" % "0.14.1"),
//      "org.apache.kafka" % "kafka_2.12" % "3.1.0" withSources() exclude("org.slf4j","slf4j-log4j12") exclude("javax.jms", "jms") exclude("com.sun.jdmk", "jmxtools") exclude("com.sun.jmx", "jmxri")),
    assemblysettings(),
    dockerSettingsProd()
  )

lazy val spark_app = (project in file("SparkApplication"))
  .enablePlugins(sbtdocker.DockerPlugin)
  .settings(
    libraryDependencies ++= Seq(
      "org.apache.spark" % "spark-streaming_2.12" % "3.2.1" % "provided",
      "org.apache.spark" % "spark-core_2.12" % "3.2.1" % "provided",
      "org.apache.spark" % "spark-sql_2.12" % "3.2.1" % "provided",
      "org.apache.spark" % "spark-sql-kafka-0-10_2.12" % "3.2.1"
    ),
    assembly / assemblyMergeStrategy := {
      case "reference.conf" => MergeStrategy.concat
      case "META-INF/services/org.apache.spark.sql.sources.DataSourceRegister" => MergeStrategy.concat
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case _ => MergeStrategy.first
    },
    dockerSettingsCons()
  )