import Domain._
import org.apache.spark.sql.{Encoders, SparkSession}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.streaming.Trigger

object SparkApplication {

  def main(args: Array[String]): Unit = {

  Logger.getLogger("org").setLevel(Level.ERROR)

  val BROKER_LIST: String = "broker:9092"
  val TOPIC: String = "PACKETS"
//  val MASTER: String = "spark://spark-master:7077"

  val spark = SparkSession.builder()
    .master("local[*]")
    .appName("kafka sink")
    .config("spark.sql.warehouse.dir", "/project/CSVs")
    .config("dfs.client.read.shortcircuit.skip.checksum", "false")
    .config("spark.executor.memory", "1024m")

    .getOrCreate()

  val PacketSchema = Encoders.product[Packet].schema

  val Lines = spark.readStream
    .format("csv")
    .option("pathGlobFilter", "*.csv")
    .option("sep", ",")
    .option("header", "true")
    .schema(PacketSchema)
    .load("/project/CSVs")

  Lines
    .selectExpr("CAST(Flow_ID AS STRING) AS key", "to_json(struct(*)) AS value")
    .writeStream
    .format("kafka")
    .outputMode("append")
    .trigger(Trigger.ProcessingTime(5))
    .option("kafka.bootstrap.servers", BROKER_LIST)
    .option("group.id", "spark-application")
    .option("truncate", "false")
    .option("checkpointLocation", "/tmp/spark/checkpoint")
    .option("topic", TOPIC)
    .start()
    .awaitTermination()

}}